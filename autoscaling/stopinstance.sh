#!/bin/bash

stopInstance() {
aws ec2 stop-instances --instance-ids $instanceId
}

for instanceId in $(aws ec2 describe-instances --filters "Name=tag:ApplicationRole,Values=jumphost" --query Reservations[]['Instances'][][][]['InstanceId'] --output text)
do
    echo $instanceId
    getASGname=$(aws autoscaling describe-auto-scaling-instances --instance-ids $instanceId --query AutoScalingInstances[]['AutoScalingGroupName'] --output text)
    if [ -z $getASGname ]
        then 
        stopInstance
    else 
        aws autoscaling suspend-processes --auto-scaling-group-name $getASGname --scaling-processes Terminate
        stopInstance
    fi
done