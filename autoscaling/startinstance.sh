#!/bin/bash

startInstance() {
aws ec2 start-instances --instance-ids $instanceId
}

for instanceId in $(aws ec2 describe-instances --filters "Name=tag:ApplicationRole,Values=jumphost" --query Reservations[]['Instances'][][][]['InstanceId'] --output text)
do
    echo $instanceId
    getASGname=$(aws autoscaling describe-auto-scaling-instances --instance-ids $instanceId --query AutoScalingInstances[]['AutoScalingGroupName'] --output text)
    if [ -z $getASGname ]
        then 
        # aws ec2 start-instances --instance-ids $instanceId
        startInstance
    else
        aws autoscaling set-instance-health --instance-id $instanceId --health-status Healthy
        aws autoscaling resume-processes --auto-scaling-group-name $getASGname --scaling-processes Terminate
        # aws ec2 start-instances --instance-ids $instanceId
        startInstance
    fi
done