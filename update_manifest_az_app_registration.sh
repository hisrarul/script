#!/bin/bash

echo "Enter the application id: "
read app_id

echo "Enter the new account id: "
read new_account_id

# Get the manifest file from az cli login
#az ad app show --id $app_id --query "appRoles" > manifest.json

# Remove the square brace from the end of the file then add comma to the last curly brace
if [ `tail -1 manifest.json` == ']' ]; then sed -i '$d' manifest.json ; sed -i '$s/$/,/' manifest.json; fi

# Update the json file with newly enter account id
account_id=`grep value update.json | cut -d':' -f6`
sed -i "s/$account_id/$new_account_id/g" update.json
echo "File update.json has been updated with `grep value update.json | cut -d':' -f6`"

# Append the content of update.json file to manifest file
cat update.json >> manifest.json

# Add square in the last new line
sed -i "\$a]" manifest.json


#Output of update.json
[root@ip-172-31-24-195 ~]# cat update.json
    {
        "allowedMemberTypes": [
          "User"
        ],
        "description": "AWS-ADMINS",
        "displayName": "AWS ADMINS Account 4",
        "isEnabled": true,
        "value": "arn:aws:iam::132132132132:role/AWS-ADMINS,arn:aws:iam::132132132132:saml-provider/Israrul"
      }

# Output of manifest.json file
[root@ip-172-31-24-195 ~]# cat manifest.json
[
  {
    "allowedMemberTypes": [
      "User"
    ],
    "description": "AWS-ADMINS",
    "displayName": "AWS ADMINS Account 3",
    "id": "da49cfd3-30cf-4c10-86e1-3fce20744373",
    "isEnabled": true,
    "value": "arn:aws:iam::817338342595:role/AWS-ADMINS,arn:aws:iam::817338342595:saml-provider/Israr"
  },
  {
    "allowedMemberTypes": [
      "User"
    ],
    "description": "AWS-ADMINS",
    "displayName": "AWS ADMINS Account 2",
    "id": "6efd756e-8c27-4472-b2b7-38c17fc51232",
    "isEnabled": true,
    "value": "arn:aws:iam::885035737937:role/AWS_ADMINS,arn:aws:iam::885035737937:saml-provider/AZUREAD"
  },
  {
    "allowedMemberTypes": [
      "User"
    ],
    "description": "AWS-ADMINS",
    "displayName": "AWS ADMINS Account 1",
    "id": "7efd756e-8c27-4472-b2b7-38c17fc51232",
    "isEnabled": true,
    "value": "arn:aws:iam::817338342595:role/AWS-ADMINS,arn:aws:iam::817338342595:saml-provider/AZUREAD"
  },
  {
    "allowedMemberTypes": [
      "User"
    ],
    "description": "msiam_access",
    "displayName": "msiam_access",
    "id": "7dfd756e-8c27-4472-b2b7-38c17fc5de5e",
    "isEnabled": true,
    "value": null
  },
    {
        "allowedMemberTypes": [
          "User"
        ],
        "description": "AWS-ADMINS",
        "displayName": "AWS ADMINS Account 4",
        "isEnabled": true,
        "value": "arn:aws:iam::123123123123:role/AWS-ADMINS,arn:aws:iam::123123123123:saml-provider/Israrul"
      }
]
