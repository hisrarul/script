$bucketname = 'resource-data-sync-hyderabad'
$account = '255026805435'
$loc = 'us-east-1'

$path = "$bucketname/AWS:InstanceInformation/accountid=$account/region=$loc/resourcetype=ManagedInstanceInventory"
$path2 = "$bucketname/AWS:InstanceDetailedInformation/accountid=$account/region=$loc/resourcetype=ManagedInstanceInventory"


foreach ($instance in aws s3 ls s3://$path/ | foreach { ($_.trim() -split '\s+')[-1] })
{

#Get instance information
aws s3 cp s3://$path/$instance .
cat $instance >> inventory.json

#Get instance detailed information
aws s3 cp s3://$path2/$instance .
cat $instance >> inventory.json
rm $instance
}


#Upload the inventory file to s3 bucket
aws s3 cp inventory.json s3://$bucketname

#Remove the file file from server
rm inventory.json