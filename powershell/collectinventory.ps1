$bucketname = 'resource'
$account = '2xxxxxxxxxx'
$loc = 'us-east-1'

$key1 = "AWS:InstanceInformation/accountid=$account/region=$loc/resourcetype=ManagedInstanceInventory"
$key2 = "AWS:InstanceDetailedInformation/accountid=$account/region=$loc/resourcetype=ManagedInstanceInventory"

$s = (Get-S3Object -BucketName $bucketname -Key $key1).Key

$m = (Get-S3Object -BucketName $bucketname -Key $key2).Key
$count=0
foreach ($element in $s) 
{
echo $element
#Copy-S3Object -BucketName $bucketname -Key $element -DestinationKey temp.json
Copy-S3Object -BucketName $bucketname -Key $element -File .\temp.json
gc .\temp.json | Add-Content -Path .\inventory.json
echo $m[$count]
#Copy-S3Object -BucketName $bucketname -Key $m[$count] -DestinationKey invent.json
Copy-S3Object -BucketName $bucketname -Key $m[$count] -File .\temp.json
gc .\temp.json| Add-Content -Path .\inventory.json
$count++

}
Write-S3Object -BucketName $bucketname -File .\inventory.json
Remove-Item ./inventory.json