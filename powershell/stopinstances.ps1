#ForStopping

$ec2 = (Get-EC2Instance -Filter @( @{name='tag:Role'; values="jumphost"})).Instances.InstanceId 
foreach ($element in $ec2) 
{ $asg = (Get-ASAutoScalingInstance -InstanceId $element).AutoScalingGroupName 
if($asg.length -ne 0)
{echo "Autoscaling process will stop"
Suspend-ASProcess -AutoScalingGroupName $asg -ScalingProcess "Terminate"
echo "Autoscaling ec2 wil stop"
Stop-EC2Instance -InstanceId $element
}else
{ echo "only EC2 will stop"
Stop-EC2Instance -InstanceId $element
}

}