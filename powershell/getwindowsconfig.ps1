systeminfo > Input.txt
echo "Info::OS Name"
cat Input.txt | findstr /B /C:"OS Name" |convertto-json
echo "Info::Create Date"
cat Input.txt | findstr /B /C:"Original Install Date"
echo "Info::Hostname and Domain"
cat Input.txt | findstr /B /C:"Host Name" /C:"Domain"
echo "Info::IP Addresses"
ipconfig | Select-String -Pattern 'IPv4 Address'
echo "Info::Available RAM in Bytes" 
Get-CimInstance win32_PhysicalMemory | findstr -i Capacity
echo "Info::Available Disk Size"
Get-CimInstance Win32_DiskDrive | Select Size|ConvertTo-json
echo "Info::CPU"
Get-WmiObject -Class Win32_Processor | Select-Object -Property Name | convertto-json
echo "Info::Number of Core"
Get-WmiObject -Class Win32_Processor | Select-Object -Property NumberofCores|ConvertTo-json
echo "Info::CPU Core Count"
Get-WmiObject -Class Win32_Processor | Select-Object -Property NumberOfLogicalProcessors | convertto-json
#Remove the Input.txt file
rm Input.txt
echo "Info::Get the Environment tag with Instance"
#InstanceId should be pass as a value
$InstanceId = 'i-068ed897508c7f0b9'
aws ec2 describe-tags --filters "Name=resource-id,Values=$InstanceId" Name=key,Values=Environment | Select-String -Pattern 'Value'
echo "Info::Region"
aws configure get default.region