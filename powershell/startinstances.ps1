#For Starting

$ec2 = (Get-EC2Instance -Filter @( @{name='tag:Role'; values="jumphost"})).Instances.InstanceId 
foreach ($element in $ec2) 
{ $asg = (Get-ASAutoScalingInstance -InstanceId $element).AutoScalingGroupName 
if($asg.length -ne 0)
{echo "Autoscaling ec2 wil start"
Start-EC2Instance -InstanceId $element
Start-Sleep -s 180
Set-ASInstanceHealth -HealthStatus Healthy -InstanceId $element
echo "Autoscaling process will start"
Resume-ASProcess -AutoScalingGroupName $asg -ScalingProcess "Terminate"
}else
{ echo "only EC2 will start"
Start-EC2Instance -InstanceId $element
}

}