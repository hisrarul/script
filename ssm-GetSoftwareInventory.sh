#!/bin/bash

bucketname=resource-data-sync
account=123456789122
loc='us-east-1'

path=$bucketname/AWS:InstanceInformation/accountid=$account/region=$loc/resourcetype=ManagedInstanceInventory/
path2=$bucketname/AWS:InstanceDetailedInformation/accountid=$account/region=$loc/resourcetype=ManagedInstanceInventory/

truncate -s 0 inventory.json
for instance in $(aws s3 ls s3://$path | awk '{print $NF}')
do

#Get instance information
aws s3 cp s3://$path$instance .
cat $instance >> inventory.json

#Get instance detailed information
aws s3 cp s3://$path2$instance .
cat $instance >> inventory.json
done

#Upload the inventory file to s3 bucket
aws s3 cp inventory.json s3://$bucketname