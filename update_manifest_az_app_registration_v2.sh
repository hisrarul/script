#!/bin/bash

echo "Enter the application id: "
read app_id

#echo "Enter the description: "
#read app_desc


#echo "Enter the diplay name: "
#read app_dis_name

echo "Enter the new account id: "
read new_account_id

# Get the manifest file from az cli login
az ad app show --id $app_id --query "appRoles" > manifest.json

# Remove the square brace from the end of the file then add comma to the last curly brace
if [ `tail -1 manifest.json` == ']' ]; then sed -i '$d' manifest.json ; sed -i '$s/$/,/' manifest.json; fi

# Update the json file with newly enter account id
account_id=`grep value update.json | cut -d':' -f6`
sed -i "s/$account_id/$new_account_id/g" update.json
echo "File update.json has been updated with `grep value update.json | cut -d':' -f6`"

# Update the app_description
# old_app_desc=`grep description update.json | cut -d'"' -f4`
# sed -i "s/$old_app_desc/$app_desc/" update.json

# Update the app display name
# old_display_name=`grep displayName update.json | cut -d'"' -f4`
# sed -i "s/$old_display_name/$app_dis_name/" update.json

# Append the content of update.json file to manifest file
cat update.json >> manifest.json

# Add square in the last new line
sed -i "\$a]" manifest.json

# Upload the manifest file
echo "Please enter 'y' to upload or press anything else."
read input

if [ $input == 'y' ]
then
az ad app update --id $app_id --app-roles @manifest.json
else
echo "Please feel free to upload later.."
fi
